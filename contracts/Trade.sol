pragma solidity ^0.4.19;

contract Trades{
    enum permissions {none, read, write}
    enum statements {text, file}
    struct Trade {
      address creator;
      string assetId;
      bool done;
      mapping (address => permissions ) participants;

    }
    Trade[] public trades;
    uint statementId;
    function makeTrade(string assetId) public {
        trades.push(Trade({
            creator: msg.sender,
            assetId: assetId,
            done: false
        }));
        trades[trades.length-1].participants[msg.sender] = permissions.write;
        NewTrade(trades.length-1,msg.sender);
        Permission(trades.length-1, msg.sender, permissions.write);
    }
    modifier isOwner (uint tradeId, address addr)  {
        require(trades[tradeId].creator == addr);
        _;
    }
    modifier hasPermission (uint tradeId, address addr) {
        require(trades[tradeId].participants[addr] != permissions.none);
        _;
    }
    modifier canWrite (uint tradeId, address addr) {
        require(trades[tradeId].participants[addr] == permissions.write);
        _;
    }
    function getTradesCount() public constant returns (uint){
        return trades.length;
    }
    function getPermission(uint tradeId, address addr) public constant returns (permissions){
        return trades[tradeId].participants[addr];
    }
    function setPermission(uint tradeId, address participantAddr, permissions p) public isOwner(tradeId, msg.sender){
        require(participantAddr != trades[tradeId].creator);
        trades[tradeId].participants[participantAddr] = p;
        Permission(tradeId, participantAddr, p);
    }

    function finishTrade(uint tradeId) public  isOwner(tradeId, msg.sender){
        trades[tradeId].done = true;
    }
    function addStatement(uint tradeId, uint statementType) public canWrite(tradeId, msg.sender){
        Statement(tradeId, msg.sender, statementId, statements(statementType));
        statementId++;
    }
    function linkTrade(uint tradeId, uint linkedTradeId) public isOwner(tradeId, msg.sender) hasPermission(linkedTradeId, msg.sender){
        linkedTrade(tradeId, linkedTradeId);
    }

    event NewTrade(uint indexed tradeId, address indexed owner);
    event Statement(uint indexed tradeId, address indexed from, uint indexed statementId, statements statementType);
    event Permission(uint indexed tradeId, address indexed participantAddr, permissions p);
    event File(uint indexed tradeId, address indexed from, uint indexed statementId, uint fileId);
    event linkedTrade(uint indexed tradeId, uint indexed linkedTradeId);

}
